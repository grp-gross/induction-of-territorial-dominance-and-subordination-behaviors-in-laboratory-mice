"""
Module for exploring the following dataset ('pca_dataset.csv')
via PCA and for plotting 2 dimensional data 
resulting from the projection into the space generate 
by the first two principal components:

|  | video          |chases|attacks|other_comp|other_cage|locomotion|ur_postures|flights|hiding|mouse|line |time|interaction|
|- |-               | -    |-      |-         |-         |-         |-          |-      |-     |-    |-    |-   |-          |
|0 |CD1-B1-UN_LS-T1 |0.0   |25.0   |9.5       |0.0       |15293.8694|6.0        |10.0   |21.5  |UN   |CD1  |T1  |1          |
|1 |CD1-B1-UN_LS-T1 |15.0  |32.0   |50.5      |12.0      |12501.2516|0.0        |1.0    |12.5  |LS   |CD1  |T1  |1          |
|2 |CD1-B1-MS_US-T1 |2.0   |25.0   |41.5      |2.0       |15990.6214|0.0        |0.0    |23.0  |MS   |CD1  |T1  |1          |
|3 |CD1-B1-MS_US-T1 |6.0   |23.0   |32.0      |1.0       |13421.5625|0.0        |5.0    |22.5  |US   |CD1  |T1  |1          |
|4 |CD1-B2-UN_LS-T1 |6.0   |38.0   |27.5      |5.5       |15003.1560|4.0        |9.0    |46.5  |UN   |CD1  |T1  |1          |
|..|...             |...   |...    |...       |...       |...       |...        |...    |...   |...  |...  |... |...        |

"""

import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler

PATH = 'pca_dataset.csv'
STRAINS = ['CD1','C57j','Hyb']


def create_subset_for_pca(line: str, time: str, interaction: int = None) -> pd.DataFrame:
    """
        Creates a subset of the entire dataset on which PCA will be performed.
        Data are selected based on the mouse strain "line" and the recording
        period "time".
        
        Args: 
            data: entire dataset from which a subset is extracted
            line: condition to select a specific strain on mice
            time: recording time to filter out early/late data
            interaction: if non None, 1 for selecting only interacting mice, 
                         0 for non-interacting mice.
        Returns: 
            dataset: subset of initial dataset
            
    """
    data = pd.read_csv(PATH)
    cond_time = data.time == time
    dataset = data[cond_time]
    
    
    if interaction: 
        cond_interaction = data.interaction == interaction
        dataset = dataset[cond_interaction]
    
    if line in STRAINS:
        cond_line = dataset.line == line
        dataset = dataset[cond_line]
    return dataset



def plot_projected_data(new_data: pd.DataFrame, status_column: str=None) -> None:
    """
        Plots data points projected into the space defined by the two principal component 
        and colors point based on their status (dominant or subordinate mouse) or mouse line
        
        Args: 
            new_data: input 2-dimensional dataset with mice as data points and 'pca0', 'pca1', 
                      'line', status' and 'interaction' as columns.
                      Mice belonging to the same pair have to appear consecutively in 
                      the dataset
            status_columns: dataset columns to be chosen as color criterion
        Returns: 
            plot color-coded two dimensional data points
    """
    if status_column == 'status_and_interaction':
        colors = {'dominant': 'purple','subordinate':'orange','no interaction':'yellow'}




        edgecolors = {'dominant': 'indigo','subordinate':'darkorange','no interaction':'gold'}
    elif status_column == 'line':
        colors = {'CD1': 'gold','Hyb':'firebrick','C57j':'deepskyblue'}
        edgecolors = {'CD1': 'goldenrod','Hyb':'maroon','C57j':'dodgerblue'}
    elif status_column == 'status':
        colors = {'dominant': 'purple','subordinate':'orange'}
        edgecolors = {'dominant': 'indigo','subordinate':'darkorange'}

    for key,value in colors.items():
        cond = new_data[status_column] == key
        data_plot = new_data[cond]
        color = value
        edgecolor = edgecolors[key]
        plt.scatter(data_plot['pca0'],data_plot['pca1'],color = color,
                    s = 200, edgecolors = edgecolor, label = key)

    plt.legend()
    plt.xlabel('First Principal Component', fontsize = 17)
    plt.ylabel('Second Principal Component', fontsize = 17)


def plot_pairs(new_data: pd.DataFrame) -> None:
    '''
        Plots a segment between two projected data points if they represent 
        two mice belonging to the same pair.
        
        Args: new_data: input 2-dimensional dataset with mice as data points and 'pca0', 'pca1', 
              'line', status' and 'interaction' as columns.
              Mice belonging to the same pair have to appear consecutively in 
              the dataset
        Returns: 
            plots segments between mice belonging to the same pair
        
    '''
    n = new_data.shape[0]
    for i in range(0,n,2):
        plt.plot([new_data.iloc[i]['pca0'],new_data.iloc[i+1]['pca0']],
                 [new_data.iloc[i]['pca1'],new_data.iloc[i+1]['pca1']],
                 color = 'grey', alpha = 0.1)

    plt.xlabel('First Principal Component', fontsize = 17)
    plt.ylabel('Second Principal Component', fontsize = 17)
    
    
def plot_loadings(v1: list,v2: list):
    """
        Plots pca loadings.
        
        Args: 
            v1: loadings for PC1
            v2: loadings for PC2
    """
    
    plt.figure(figsize=(5,3))
    plt.bar([i for i in range(len(v1))],v1,color = 'teal', width = 0.8, alpha = 0.3,label = 'PC1')
    plt.bar([i for i in range(len(v2))],v2,color = 'brown', width = 0.5, alpha = 0.4,label = 'PC2')
    plt.vlines([i for i in range(len(v1))],-1,1,color = 'grey', linewidth = 1, alpha = 0.2)
    plt.hlines(0,-10,10,color = 'grey', linewidth = 1, alpha = 0.2)
    
    numerical_columns = ['chases','attacks','other_comp','locomotion',
                         'ur_postures','flights','hiding']
    plt.xlim((-0.7,7.7))
    plt.xticks([i for i in range(len(v1))],numerical_columns,rotation = 45)

    plt.title('Factor loadings for PC1 and PC2')
    plt.legend()
    #plt.savefig('Pictures/loadings.svg')
    plt.show()

    
    
    
try:
    print('\nPerforming PCA analysis')
    dataset = create_subset_for_pca('all animals','T2')
        
    numerical_columns = ['chases','attacks','other_comp','locomotion',
                         'ur_postures','flights','hiding']
    
    scaler = StandardScaler().set_output(transform = "pandas")
    scaled_dataset = scaler.fit_transform(dataset[numerical_columns])
       
    pca = PCA(n_components=scaled_dataset.shape[1]).set_output(transform="pandas")
    pca.fit(scaled_dataset)
    new_data = pca.transform(scaled_dataset)
    print('explained variance: ', [round(i,3) for i in pca.explained_variance_ratio_])
    
    new_data[['video','mouse','line',
              'time','interaction']] = dataset[['video','mouse','line',
                                                'time','interaction']]
    plt.figure(figsize=(7,7))
    plot_projected_data(new_data,'line')
    plot_pairs(new_data)
    plt.xlim((-4.7,6.3))
    plt.ylim((-2.3,5.5))
    plt.title('PCA', fontsize = 20)
    plt.show()
    
    
    videos = list(new_data.video.unique())
    new_status = []
    for video in videos:
        if new_data.pca0[new_data.video == video].values[0] > new_data.pca0[new_data.video == video].values[1]:
            new_status.extend(['dominant','subordinate'])
        else: 
            new_status.extend(['subordinate','dominant'])
    new_data['status'] = new_status
    
    v1 = pca.components_[0,:]
    v2 = pca.components_[1,:]
    plot_loadings(v1,v2)
    
    print()
except: 
    print('Failed')