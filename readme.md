

# Induction of Territorial Dominance and Subordination Behaviors in Laboratory Mice

This repository contains the code used for analyzing the research described in the paper by
Dorian Battivelli, Lucas Boldrini, Mohit Jaiswal, Pradnya Patil, Sofia Torchia, Elizabeth Engelen, Luca Spagnoletti, Sarah Kaspar and Cornelius T. Gross: "**Induction of Territorial Dominance and Subordination Behaviors in Laboratory Mice**".<br>

#### Abstract

Territorial behaviors comprise a set of coordinated actions and response patterns found across animal species that promote the exclusive access to resources. House mice are highly territorial with a subset of males consistently attacking and chasing competing males to expel them from their territories and performing urine marking behaviors to signal the extent of their territories. Natural variation in territorial behaviors within a mouse colony leads to the formation of dominance hierarchies in which subordinate males can reside within the territory of a dominant male. While the full repertoire of such territorial behaviors and hierarchies has been extensively studied in wild-derived mice in semi-natural enclosures, so far they have not been established in the smaller enclosures and with the genetically-defined laboratory strains required for the application of neural recording and manipulation methods. Here, we present a protocol to induce an extensive repertoire of territorial behaviors in small enclosures in laboratory mice, including a method for the simultaneous tracking of urine marking behavior in mouse pairs. Using this protocol we describe the emergence of robust dominant-subordinate hierarchies between pairs of CD1 outbred or CD1xB6 F1 hybrid mice, but  unexpectedly not in C57BL/6 inbred animals. Our behavioral paradigm opens the door for neurocircuit studies of territorial behaviors and social hierarchy in the laboratory.

## Overview

The repository includes code for the following analyses: <br>
1. Average Distance and Speed:
    - ```window_analysis.py``` computes the distance between two mice and the speed of a single mouse over time.
    - ```plot_window.py``` generates plots showing overlapping distance or speed curves extracted from windows centered around all flight onsets.
2. Permutation test for comparison with baseline interaction
3. Unbiased Dominance Score via PCA Analysis
4. Proximity measure
5. Time Spent Hiding

### Nomenclature
In all data files, each mouse pair is identified by their **strain** label (CD1, C57j or Hyb), a label indicating a specific **batch** (B1, B2, ..) followed by the **pair** nomenclature (MS_US, UN_LN, ..) and the **recording time** (T1.1, T1.2, T2.1 or T2.2). <br>
Examples: CD1-B1-MS_US-T1.1, C57j-B4-UN_LS-T2.1, Hyb-B3-MS_US-T2.2

The following **pair** nomenclature has been employed: <br>
unshaved: “UN”; low shaved: “LS”; middle shaved: “MS”; up shaved: “US”. <br>
In addition to this, each mouse is identified by either "M1" or "M2" labels, depending on the order its label occurs in the pair nomenclature. For example, in CD1-B3-MS_US-T2.1, MS corresponds to M1 and US corresponds to M2.

The **recording time** refers to the recording time interval within the early (T1) or the late (T2) experimental phase. Both T1 and T2 intervals are 20 minute-long periods which have been divided up into two consecutive sub-intervals of 10 minutes (T1.1 and T1.2 for T1; T2.1 and T2.2 for T2). 



### 1) Average distance and speed 
```window_analysis.py``` implements the necessary steps to compute distance between two mice and speed of a mouse over time, while ```plot_window.py``` produces plots for overlapping distance or speed curves extracted from windows centered around all flight onsets. <br>
Here, input data (behavioral recordings and coordinates
files) are a .csv files stored in a folder named respectively
"Data" and "Behavioral scoring". We also assign each mouse a dominance status ("dominant" or "subordinate") through ```Labels.csv```.

In order to run this code, organize data and code as follows:
- Data:
    - ```CD1-B1-MS_US-T1.1.csv```
    - ```CD1-B1-MS_US-T1.2.csv```
    - ...  
- Behavioral scoring:
    - ```CD1-B1-MS_US-T1.1.csv```
    - ```CD1-B1-MS_US-T1.2.csv```
    - ...
- ```Labels.csv```
- ```window_analysis.py```
- ```plot_window.py```


    
Each mouse is originally identified by the x and y coordinates of eight points on its body. These coordinates are tracked over the whole recording and stored in a .csv file in "Data":

| <span style="color:grey">UM_Ear_left_1_x</span> | <span style="color:grey">UM_Ear_left_1_y</span> | <span style="color:grey">UM_Ear_left_1_p</span> | <span style="color:grey">UM_Nose_1_x</span> | <span style="color:grey">UM_Nose_1_y</span> | ... | <span style="color:grey">LM_Lat_left_2_p</span> | <span style="color:grey">LM_Lat_right_2_x</span> | <span style="color:grey">LM_Lat_right_2_y</span> | <span style="color:grey">LM_Lat_right_2_p</span> | <span style="color:grey">LM_Tail_base_2_x</span> | <span style="color:grey">LM_Tail_base_2_y</span> | <span style="color:grey">LM_Tail_base_2_p</span> |
|-------------------------|-------------------------|-------------------------|--------------------|--------------------|-----|-------------------------|--------------------------|--------------------------|--------------------------|---------------------------|---------------------------|---------------------------|
| 1174.5143               | 247.42857               | 1.0                     | 1139.6571          | 258.20000          | ... | 1.0                     | 1968.7428                | 1937.9714                | 1.0                      | 1932.0857                | 1974.3429                | 0.999257                  |
| 1175.5714               | 248.74286               | 1.0                     | 1139.6000          | 254.71428          | ... | 1.0                     | 1968.1714                | 1935.8572                | 1.0                      | 1931.7428                | 1974.4857                | 0.999743                  |
| 1176.0000               | 249.91429               | 1.0                     | 1139.4000          | 252.65715          | ... | 1.0                     | 1968.1714                | 1935.1714                | 1.0                      | 1931.2572                | 1974.3429                | 1.000086                  |
| 1174.8286               | 250.17143               | 1.0                     | 1139.2572          | 253.82857          | ... | 1.0                     | 1968.8286                | 1936.5714                | 1.0                      | 1930.9143                | 1973.9143                | 1.000000                  |
| 1174.6857               | 249.82857               | 1.0                     | 1139.2572          | 253.74286          | ... | 1.0                     | 1968.8286                | 1936.9143                | 1.0                      | 1931.0857                | 1973.9143                | 1.000000                  |
|...|...|...|...|...|...|...|...|...|...|...|...|...|...|




Each behavioral annotation file in "Behavioral scoring" is similar to:
|     |<span style="color:grey">Time</span>  |  <span style="color:grey">Default</span>|
| -   | - |  -|
|0    |0.00   | NaN|
|1    |0.20  |  NaN|
|2    |0.60  |  C-M2-C1 |    # mouse M2 chases (C) mouse M1 in compartment C1
|3    |0.80  |  NaN|
...  |...    | ...|

```Labels.csv``` has the following structure:
|    |    <span style="color:grey">Video</span>                   |  <span style="color:grey">Dominant</span> |    <span style="color:grey">Subordinate</span> |   <span style="color:grey">Start_frame</span>|
|-|-|-|-|-|
|0     |  C57j-B1-MS_US-T1.1.csv  |  M2        |   M1          |    495|
|1   |    C57j-B1-UN_LS-T1.1.csv  |  M1       |    M2          |    0|
|2   |    C57j-B2-MS_US-T1.1.csv  |  M1       |    M2         |     675|
|3   |    C57j-B2-UN_LS-T1.1.csv  |  M2       |    M1          |    0|
|... |    ...                     |  ...      |    ...         |    ...|


Behavior is annotated at 5 frames per second, 
while coordinates over time are extracted at 45 frames per second. For each behavioral scoring file the first frames are discarded; ```Labels.csv```stores the actual initial frame for these files inside the column "Start_frame".

### 2) Permutation test for comparison with baseline interaction
The permutation tests run on an `.xlsx` data file, which has the columns `Mouse_line`, `pair`, and one column for each behavior to be tested.

### 3) Unbiased dominance score via PCA analysis
File organization:
- pca_analysis.py
- pca_dataset.csv






The script pca_analysis.py operates on pca_dataset.csv which has the following structure:

| | <span style="color:grey">video&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span>|	<span style="color:grey">chases</span>|<span style="color:grey">attacks</span>|<span style="color:grey">other_comp</span>|<span style="color:grey">other_cage</span>|<span style="color:grey">locomotion</span>|	<span style="color:grey">ur_postures</span>|<span style="color:grey">flights</span>|<span style="color:grey">hiding</span>|<span style="color:grey">mouse</span>|<span style="color:grey">line</span>|<span style="color:grey">time</span>|<span style="color:grey">interaction</span>|
|-  |-              | -|-        |-      |     -|-             |-       |-       |-      |-    | -  |-     |-    |
|0 |CD1-B1-UN_LS-T1|0.0| 	25.0 |	9.5 |	0.0 |	15293.8694 |	6.0 |	10.0 |	21.5 |	UN |CD1 |	T1 |	1|
|1 |CD1-B1-UN_LS-T1|15.0| 	32.0 |	50.5 |	12.0 |	12501.2516| 	0.0 |	1.0 |	12.5 |LS 	|CD1| 	T1 |   1|
|2 |CD1-B1-MS_US-T1|2.0 |	25.0 |	41.5 |	2.0 |	15990.6214 |	0.0 |	0.0 |	23.0 |	MS 	|CD1 |	T1 | 	1|
|3 |CD1-B1-MS_US-T1|6.0| 	23.0 |	32.0 |	1.0 |	13421.5625 |	0.0| 	5.0 |	22.5 |	US 	|CD1 |	T1 |	1|
|...|...|...|...|...|...|...|...|...|...|...|...|...|...|


### 4 - 5) Proximity measure and Hiding analysis

Both analysis require the following data format:

| |<span style="color:grey">video</span>| 	<span style="color:grey">animal</span> |	<span style="color:grey">roi</span>| 	<span style="color:grey">entry</span>| 	<span style="color:grey">exit</span>|
|-|-|-|-|-|-|
|0 |	C57j-B6-US_MS |	UM |	Up-corn1 |	1214 |	1477|
|1 |	C57j-B6-US_MS| 	UM |	Up-corn1 |	1690 |	2500|
|2 |	C57j-B6-US_MS |	UM |	Up-corn1 |	2570 |	2598|
|3 |	C57j-B6-US_MS |	UM |	Up-corn1 |	3688 |	3737|

The script for "proximity measure" quantifies the time two mice from the same pair spend close to each other. "Close" is defined by being in the same ROI and having an Euclidean distance less than a predefined threshold (here 10 centimeters), referred to as the "radius."<br>
The script for "hiding" computer for each mouse belonging to the same pair: 
- the total time of the recording
- the total time spent hiding
- the portion of time spent in various regions (e.g. other compartment, other nest, other HF) <br><br>




## Running this code
Ensure Python version 3.10.0 is installed to execute the provided scripts successfully, and that all libraries listed in ```requirements.txt``` are available with specified versions. 
You can do so by cloning this repository and running the following commands: <br>

```
conda create --name behavior_analysis python==3.10
conda activate behavior_analysis
pip install -r requirements.txt
```
You can now run the analysis for speed and distance from the repository directory by typing:
```
python plot_window.py
```
And the PCA exploration for dominance labeling with:
```
python pca_analysis.py
```

### Permutation tests


For running the analysis 

- make sure you have the libraries `tidyverse` and `readxl` installed 
- navigate to the folder that contains `permutations.R` 

Run the script as follows:

```console
Rscript --vanilla permutations.R INPUT_FILE OUTPUT_FILE
```
 
where `INPUT_FILE` gives the path to an `.xlsx` table containing the data to be analyzed, and `OUTPUT_FILE` is the path to store the results. The output file should be `.tsv`.

Optionally, you can provide a seed as the third argument, and the number of permutations as the fourth argument. For example:

```console
Rscript --vanilla permutations.R data/T1.xlsx results/T1-results.tsv 22 1000
```

For this publication, the analysis was run with `seed = 21` and `N=10000` permutations, with R version 4.3.0 under macOS 14.4.1.

## Authors
Sofia Torchia <br>
Sarah Kaspar





