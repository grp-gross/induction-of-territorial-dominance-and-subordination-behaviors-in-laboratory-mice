""" 
Description:
This module implements the necessary steps to compute
distance between two mice and speed of a mouse over time.


File organization:
Here, we assume that behavioral recordings and coordinates
files are a .csv file stored in a folder named respectively
Data and Behavioral scoring.
We also assign each mouse a dominance status ("dominant" or "subordinate") 
through Labels.csv

In order to run this code, data and code have to be organized as follows:
- Data:
    - CD1-B1-MS_US-T1.1.csv
    - CD1-B1-MS_US-T1.2.csv
    - ...  
- Behavioral scoring:
    - CD1-B1-MS_US-T1.1.csv
    - CD1-B1-MS_US-T1.2.csv
    - ...
- Labels.csv
- window_analysis.py
- plot_window.py

    
    

Data structure:
Each coordinate file in Data has a structure similar to:
        MS_Ear_left_1_x      MS_Ear_left_1_y       ...           US_Tail_end_2_y
0       1174.5143            247.42857             ...           1964.00000
1       1175.5714            248.74286             ...           1964.08570
2       1176.0000            249.91429             ...           1963.74280
3       1174.8286            250.17143             ...           1963.25720
4       1174.6857            249.82857             ...           1962.82860
...     ...                  ...                   ...           ...

Each behavioral file in Behavioral scoring folder is similar to:
     Time    Default
0    0.00    NaN
1    0.20    NaN
2    0.40    NaN
3    0.60    C-M2-C1     # mouse M2 chases (C) mouse M1 in compartment C1
4    0.80    NaN
...  ...     ...

Labels.csv has the following structure:
        Video                     Dominant     Subordinate    Start_frame
0       C57j-B1-MS_US-T1.1.csv    M2           M1              495
1       C57j-B1-UN_LS-T1.1.csv    M1           M2              0
2       C57j-B2-MS_US-T1.1.csv    M1           M2              675
3       C57j-B2-UN_LS-T1.1.csv    M2           M1              0
4       C57j-B3-MS_US-T1.1.csv    M1           M2              450
...     ...                       ...          ...             ...

Frame rate:
Behavioral scoring is annotated at 5 frames per second, 
while coordinates over time are extracted at 45 frames per second.


"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import rcParams


MOUSE1 = 'M1'
MOUSE2 = 'M2'
RIGHT = 60
LEFT = 30
FRAMES_PER_SECOND = 45
FRAME_RATE_ADAPT = 9
PIXEL_TO_CM = 15.4
MAX_LENGTH = 3000

BEHAVIORS = ['Fl','A','C','U']  # list of annotated behaviors
LABELS_PATH = 'Labels.csv'    # stores dominance status for all mice


'''
 TODO:
1) delete unused functions
3) constants
4) add comments and examples
5) find better names for functions
6) strain or line or both?
10) function order
13) append or extend
'''



def get_data(video: str) -> pd.DataFrame:
    '''
        This function processes a given video recording and extracts 
        the positional data of two mice over time. 
        Specifically, the output dataset contains the coordinates 
        along the x and y axes for the eight points identifying each mouse.
        
        Args:
            video: name of recording stored in Data folder
        Returns: 
            pandas dataframe corresponding to mice coordinates extracted
            from the recording
    '''
    data = pd.read_csv('Data/' + video, header = 0)
    return data



def compute_distance(data: pd.DataFrame) -> pd.Series:
    '''
        This function computes the euclidean distance over time between MOUSE1 and MOUSE2.

        Each mouse is originally identified by the x and y coordinates
        of eight points on its body, for instance mouse1 is identified by
        mouse1_x and mouse1_y data.
        For each mouse we compute its mean position over time along
        x and y coordinates, (for example: pos_mouse_1_x and pos_mouse_1_y for mouse1)
        so that one mouse is identified only by a single two-dimensional point.
        Finally, the euclidean distance between these 2 two-dimensional point over time is returned.
        
        Args:
            data: dataset with mice coordinates
        Returns:
            time series corresponding to distance between mice over time
    '''
    mouse1_x, mouse1_y = get_column_names(data, MOUSE1)
    mouse2_x, mouse2_y = get_column_names(data, MOUSE2)

    pos_mouse_1_x = data[mouse1_x].mean(axis = 1).values
    pos_mouse_1_y = data[mouse1_y].mean(axis = 1).values
    pos_mouse_1 = pd.DataFrame({'pos_x':pos_mouse_1_x,'pos_y':pos_mouse_1_y})

    pos_mouse_2_x = data[mouse2_x].mean(axis = 1).values
    pos_mouse_2_y = data[mouse2_y].mean(axis = 1).values
    pos_mouse_2 = pd.DataFrame({'pos_x':pos_mouse_2_x,'pos_y':pos_mouse_2_y})

    return ((pos_mouse_1 - pos_mouse_2)**2).sum(axis=1)**0.5



def get_column_names(data: pd.DataFrame, mouse_label: str) -> pd.DataFrame:
    '''
       Each mouse is originally identified by the x and y coordinates
       of eight points on its body, such as tail_base, tail_end,
       ear_left, ear_right, etc.
       Given a mouse_label (MOUSE1 or MOUSE2), this function returns
       the columns names of the x and y coordinates of these eigth points.
       
       Args: 
           data: dataset with mice coordinates over time
           mouse_label: name of one specific mouse
       Returns:
           colums of dataset corresponding to coordinates
           of one specific mouse
    '''
    if mouse_label == MOUSE1:
        columns =  get_column_names_lbl(data, '_1_x', '_1_y')
    if mouse_label == MOUSE2:
        columns =  get_column_names_lbl(data, '_2_x', '_2_y')
    return columns


def get_column_names_lbl(data: pd.DataFrame, x_lbl: str, y_lbl: str) -> tuple[list, list]:
    '''
        Returns list of data columns name related to x and y coordinates of a mouse.
        
        Args:
            data: dataset with mice coordinates over time
            x_lbl: label for x coordinates
            y_lbl: label for y coordinates
        
        Returns:
            mouse-specific columns names for x and y coordinates
    '''
    mouse_x = [x for x in data.columns if x_lbl in str(x)]
    mouse_y = [x for x in data.columns if y_lbl in str(x)]
    return mouse_x, mouse_y




def compute_speed(data : pd.DataFrame, mouse_label: str) -> pd.Series:
    '''
        This function computes the speed over time of a mouse.

        Each mouse is originally identified by the x and y coordinates of
        eight points on its body: mouse_x and mouse_y.
        First, these eight points are used to compute a mean two-dimensional
        point on the mouse with coordinates pos_mouse_x and pos_mouse_y.
        The mouse speed at time t is computed as the variation
        of the mouse position from time t1=t-FRAMES_PER_SECOND to time t2=t
        
        Args:
           data: dataset with mice coordinates over time
           mouse_label: name of one specific mouse

        Returns:
           speed of mouse over time
    '''

    mouse_x, mouse_y = get_column_names(data,mouse_label)

    pos_mouse_x = data[mouse_x].mean(axis = 1)
    pos_mouse_y = data[mouse_y].mean(axis = 1)

    vel_mouse_x = pos_mouse_x - pos_mouse_x.shift(FRAMES_PER_SECOND)
    vel_mouse_y = pos_mouse_y - pos_mouse_y.shift(FRAMES_PER_SECOND)

    speed_mouse = (vel_mouse_x**2 + vel_mouse_y**2).apply(lambda x: np.sqrt(x))
    return speed_mouse




def get_dominant(video: str) -> str:
    '''
        This function returns the dominant mouse name
        (M1 or M2) for each couple
        
        Args:
            video: name of a recording

        Returns:
            name of dominant mouse in the couple
    '''
    labels = pd.read_csv(LABELS_PATH)
    return labels[labels.Video == video]['Dominant'].values[0]




def get_subordinate(video: str) -> str:
    '''
        This function returns the subordinate mouse name
        (M1 or M2) for each couple

        Args:
            video: name of a recording

        Returns:
            name of subordinate mouse in the couple

    '''
    labels = pd.read_csv(LABELS_PATH)
    return labels[labels.Video == video]['Subordinate'].values[0]



def get_video_list() -> list:
    '''
        This function returns a list of the couples for which
        a dominant/subordinate labeling is available.
    '''
    labels = pd.read_csv(LABELS_PATH)
    return labels.Video.values.tolist()




def compute_window(video_list: list, target_behavior: str, left_length: int,
                   right_length: int, curve_to_visualize: str,
                   align_criterion: str=None) -> tuple[list, dict]:
    '''
        For each video of mice,
        this function produces windows from the time series curve_to_visualize
        of length l = left_length + right_length + 1
        centered in all events occurred for a particular behavior (chase or flight)
        taking into account the status of the fleeing mouse (dominant or subordinate)
        The selected windows are aligned following a criterion (align_criterion)
        and returned together with the mean curve.
        
        Args:
            video_list: list of recordings from which distance or speed curves
                are extracted
            target_behavior: specifies the kind of behavioral event around which 
                distance and speed windows are computed
            left_length: right length of the window
            right_length: left length of the window
            curve_to_visualize: curves to be visualized in the plot (speed or distace) 
            align_criterion: re-calculation of the 'zero' of the windows based on
                a specific criterion

        Returns:
            list of curves to visualize
    '''
    curve_list = []

    for video in video_list:
        new_list = compute_window_video(left_length, right_length,
                                        video, target_behavior,
                                        curve_to_visualize,
                                        align_criterion)
        curve_list.extend(new_list)

    curve_list = np.array(curve_list)
    return curve_list




def compute_window_video(left_length: int, right_length: int,
                         video, target_behavior: str, curve_to_visualize: str,
                         align_criterion: str=None) -> tuple[list, dict]:
    '''
        Given a time_series and a set of indices, this function produces a window
        of length l = left_length + right_length + 1
        centered in all events occurred for a particular behavior (chase or flight)
        considering the status of the fleeing mouse (dominant or subordinate)
        The selected windows are aligned together following a criterion (align_criterion)
        and returned together with the mean curve.
        
        Args:
            video: recording from which distance or speed curves
                are extracted
            target_behavior: specifies the kind of behavioral event around which 
                distance and speed windows are computed
            left_length: right length of the window
            right_length: left length of the window
            curve_to_visualize: curves to be visualized in the plot (speed or distace) 
            align_criterion: re-calculation of the 'zero' of the windows based on
                a specific criterion

        Returns:
            list of curves to visualize
    '''
    
    dominant = get_dominant(video)
    subordinate = get_subordinate(video)
    data = get_data(video)
    behavioral_scoring_data = get_behavioral_scoring_data(video, ['Fl','C'])
    speed_subordinate = compute_speed(data,subordinate) / PIXEL_TO_CM
    

    frames = get_flights(dominant, subordinate,
                         behavioral_scoring_data, target_behavior) 
    new_frames = indices_by_align_criterion(speed_subordinate,
                                            frames, align_criterion)
    time_series = get_curve_to_visualize(curve_to_visualize, data, video)

    curve_list = []
    cumsum = 0

    dataset = time_series
    for i in range(- left_length, right_length + 1, 1):
        new_col = time_series.shift(-i)
        new_col = new_col.rename(f'shift{i}')
        dataset = pd.concat([dataset, new_col], axis = 1)
    dataset.drop(columns = [0], inplace = True)

    for frame in new_frames:
        curve = dataset.loc[frame]
        cumsum = cumsum + curve['shift0']
        curve_list.append(list(curve))

    return curve_list




def get_curve_to_visualize(curve_to_visualize: str,
                           data: pd.DataFrame, video: str) -> pd.DataFrame:
    '''
        Return time series to be decomposed in compute_window(...) and represented
        through plot_window(...)
        
        Args: 
            curve_to_visualize: name of the curve to use for window computation
            data: set of mice coordinates to compute distance or speed over time
            video: name of the recording from which the mice coordinates are
                extracted.
        Returns:
            time series from which windows are computed
    '''
    if curve_to_visualize == 'distance':
        curve =  compute_distance(data) / PIXEL_TO_CM
    if curve_to_visualize == 'speed subordinate mouse':
        curve =  compute_speed(data,get_subordinate(video)) / PIXEL_TO_CM
    if curve_to_visualize == 'speed dominant mouse':
        curve =  compute_speed(data,get_dominant(video))  / PIXEL_TO_CM
    return curve




def adapt_frame_rate(old_dataset: pd.Series, behavior_list: list) -> pd.DataFrame:
    '''
        This function performs an oversampling of the behavioral dataset (old_dataset)
        from 5 frames per second to 45 frames per second, such that no behavior event
        is duplicated.

        Args:
            old_dataset: raw dataset with behavioral annotation (frame rate of 5Hz)
            behavioral_list: list of all possible behavioral annotations
        Returns:
            oversampled behavioral dataset with frame rate of 45Hz

        Example:
        old_dataset = [None, 'Flight', None]
        new = [None, None, None, None, None, None, None, None, None,
               'Flight', None, None, None, None, None, None, None, None,
               None, None, None, None, None, None, None, None, None]
    '''
    new = []
    for i in range(old_dataset.shape[0]):
        if old_dataset[i] in behavior_list:
            new.append(old_dataset[i])
            new.extend([None for _ in range(FRAME_RATE_ADAPT-1)])
        else:
            new.extend([None for _ in range(FRAME_RATE_ADAPT)])
    return pd.DataFrame({'Default': new})




def get_behavioral_scoring_data(video: str, behavior_list: list):
    '''
        Givena a couple (video), this function returns
        the dataset containing the behavioral scoring, which is at 5 Hz. 
    '''
    scoring_list = get_scoring_list(behavior_list)
    behavioral_scoring_data = pd.read_csv('Behavioral scoring/' + video)

    cond = behavioral_scoring_data.Time == 'Frequencies'
    right_length_index = behavioral_scoring_data[cond].index[0] - 1

    labels = pd.read_csv(LABELS_PATH)
    init_index = labels[labels.Video == video]['Start_frame'].values[0]
    
    right_length_index = min(right_length_index, init_index + MAX_LENGTH) 
   

    behavioral_scoring_data = behavioral_scoring_data[init_index:right_length_index].reset_index()
    behavioral_scoring_data = adapt_frame_rate(behavioral_scoring_data.Default,scoring_list)
    return behavioral_scoring_data



def index_at_behavior(beh: str, mouse: str, dataset: pd.Series) -> list:
    '''
        Given a mouse and a behavior label (beh), this function returns
        the indices at which the behavioral event occurs for the given
        mouse in both compartments (C1 and C2).
        
        Args: 
            beh: name of bahavior
            mouse: name of mouse
            dataset: dataset containing mice coordinates for a single recording
        Returns:
            list of indices where all behavioral event of the same kind and
            for specified mouse occur in the recording
    '''
    indices = []
    for comp in ['-C1','-C2']:
        condition = dataset == beh + '-' + mouse + comp
        ind = dataset[condition].index.values.tolist()
        indices.extend(ind)
    return indices




def get_scoring_list(behaviors: list) -> list:
    '''
        Returns all (complete) behavior labels for the given behaviors in input.
        
        Args: 
            behaviors: list of behavior names
        Returns:
            all possible behavioral annotation for input behavior names
            (including information about the mouse doing the behavior 
            and the compartment in which the event occurs)

        Example:
        behaviors = ['Fl','C']
        behavior_list = ['Fl-M1-C1','Fl-M1-C2','Fl-M2-C1','Fl-M2-C2',
                         'C-M1-C1','C-M1-C2','C-M2-C1','C-M2-C2']
    '''
    behavior_list = []
    for mouse in ['-M1','-M2']:
        for comp in ['-C1','-C2']:
            for behavior in behaviors:
                behavior_list.append(behavior + mouse + comp)
    return behavior_list




def get_flights(dominant: str, subordinate: str, dataset: pd.Series, behavior: str) -> list:
    '''
        Returns the event indices for:
        - all flights occurred for mouse_for_flights
        - all flights occurred for mouse_for_flights when this is NOT chased
          by mouse_for_chases whithin a time span given by time_span
        - all flights occurred for mouse_for_flights ONLY when this is chased
          by mouse_for_chases whithin a time span given by time_span
          
    Args: 
        dominant: name of dominant mouse
        subordinate: name of subordinate mouse
        dataset: dataset with behavioral annotations
        behavior: type of behavioral events
    Returns:
        list of frames when the specified type of behavior occurs
    '''
    time_span = FRAMES_PER_SECOND*2
    for i in range(-time_span,1,1):
        new_col = dataset.Default.shift(-i)
        new_col = new_col.rename('shift' + str(i))
        dataset = pd.concat([dataset, new_col], axis = 1)

    cond1 = dataset.Default == 'Fl-' + subordinate + '-C1'
    cond2 = dataset.Default == 'Fl-' + subordinate + '-C2'

    filtered = dataset[cond1 | cond2].drop(columns = ['Default'])
    cond3 = filtered.where(filtered == 'C-' + dominant + '-C2')
    cond4 = filtered.where(filtered == 'C-' + dominant + '-C1')
    flights_with_chases = filtered[cond3.any(axis = 1)].index.tolist()
    flights_with_chases = flights_with_chases + filtered[cond4.any(axis = 1)].index.tolist()

    if behavior == 'flights_with_chases':
        return flights_with_chases

    all_flights = index_at_behavior('Fl', subordinate, dataset.Default)
    if behavior == 'all_flights':
        return all_flights

    if behavior == 'flights_without_chases':
        return [f for f in all_flights if f not in flights_with_chases]







def indices_by_align_criterion(time_series: pd.Series,
                               frames: list, align_criterion: str) -> list:
    '''
        This function takes as input a time series and a set of indices,
        selects a window of the time series around each index and
        computes a new index based on one of the following criteria:
            - None: the previous index is returned
            - peak: the new index is the point of maximum of the time series
            inside the window
            - max_slope: the new index is the point of maximun slope of the time series
            inside the window
        
        Args: 
            time_series: time series based on which the new frames are computed
            frames: list of old frames
            align_criterion: criterion for calculation of the new set of frames
        Returns:
            list of new frames
    '''
    if align_criterion:
        new_frames = []
        for frame in frames:
            curve =  time_series.loc[frame - LEFT:frame + RIGHT]
            if align_criterion == 'peak':
                new_frame = curve.argmax() + max(frame - LEFT,0)
            if align_criterion == 'max_slope':
                gradient = np.gradient(curve)
                new_frame = gradient.argmax() + max(frame - LEFT,0)
            new_frames.append(new_frame)
    else:
        new_frames = frames
    return new_frames


