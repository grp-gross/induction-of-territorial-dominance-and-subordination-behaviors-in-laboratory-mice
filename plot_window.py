"""
Module for plotting overlapping distance or speed curves extracted from
windows centered around all flight onsets.
    
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import rcParams
import window_analysis as w
from scipy.stats import sem


MOUSE1 = 'M1'
MOUSE2 = 'M2'
RIGHT = 60
LEFT = 30
FRAMES_PER_SECOND = 45
FRAME_RATE_ADAPT = 9
PIXEL_TO_CM = 15.4
MAX_LENGTH = 3000

BEHAVIORS = ['Fl','A','C','U']
LABELS_PATH = 'Labels.csv'




def plot_behavioral_data(video: str) -> None:
    '''
        This function plots overlapping distance and speed traces of one pair of
        mice for a single recording as long as vertical lines correspondig to
        behavioral events (attack, flight, chase, upright posture).
        These vertical lines are colored according to the bevahior
        with _get_behavior_color(...).
        Line style is chosen based on the mouse (M1 or M2) with _get_line_style(...)

        Args:
            video: name of the recording from which distance and speed are extracted
        Returns: 
            plot  distance and speed curves
    '''

    data = w.get_data(video)
    behavioral_scoring_data = w.get_behavioral_scoring_data(video, BEHAVIORS)
    beh_list = w.get_scoring_list(BEHAVIORS)
    dist = w.compute_distance(data)
    speed_mouse_1 = w.compute_speed(data,'M1')
    speed_mouse_2 = w.compute_speed(data,'M2')

    step = 3000
    end = data.shape[0]

    for i in range(0, end, step):
        plt.figure(figsize=(25, 3))

        for beh in beh_list:
            color = _get_behavior_color(beh)
            style = _get_line_style(beh)
            func = lambda x: 3000 if x == beh else -100
            plt.plot(behavioral_scoring_data.Default[i:i+step].apply(func),
                     label=beh, color=color, linestyle = style)

        plt.plot(dist[i:i+step]/PIXEL_TO_CM, label='Mean distance', color='black')
        plt.plot(speed_mouse_1.loc[i:i+step]/PIXEL_TO_CM,
                 label='speed mouse 1', color='orange', alpha=1)
        plt.plot(speed_mouse_2.loc[i:i+step]/PIXEL_TO_CM,
                 label='speed mouse 2', color='purple', alpha=1)

        plt.ylim((0, 150))
        plt.xlabel('Frames', fontsize=15)
        plt.ylabel('Distance (cm) / Speed (cm/s)', fontsize=15)
        plt.axhline(y=300, color='black', linestyle='--')
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.show()
        

def _get_behavior_color(behavior: str) -> str:
    '''
        Returns the color to be used for the vertical lines in
        plot_behavioral_data(...) based on the behavior the lines represent

        Args:
            behavior: type of behavior
        Returns:
            color name for vertical line
    '''
    if 'R' in behavior:
        color = 'grey'
    if 'Fl' in behavior:
        color = 'deepskyblue'
    if 'A' in behavior:
        color = 'green'
    if 'Cl' in behavior:
        color = 'red'
    if 'C-' in behavior:
        color = 'chartreuse'
    else:
        color = 'blue'
    return color


def _get_line_style(behavior: str) -> str:
    '''
        Returns line style for vertical lines in plot_behavioral_data(..)

        Args:
            behavior: type of behavior
        Returns:
            line style for vertical line
    '''
    if 'M1' in behavior:
        style = 'dotted'
    else:
        style = 'solid'
    return style
    

    
def plot_behavioral_data_example(start: int, end: int, video: str) -> None:

    '''
        This function plots distance and speed traces of one pair of mice for
        a single recording separately, for a subset of the entire recording,
        between frame=start and frame = end.
        Both distance between mice and the pair of speed curves are
        represented together with the vertical lines correspondig to behavioral
        events (attack, flight, chase, upright posture).
        These lines are colored according
        to the bevahior with _get_behavior_color(...).
        Line style is chosen based on the mouse (M1 or M2)
        with _get_line_style(...)
        
        Args: 
            start: start value for x ticks
            end: end value for x ticks
            video: name of the recording from which speed and distance
                are extracted
                
        Returns:
            plot of distance and speed curves
    '''

    data = w.get_data(video)
    
    behavioral_scoring_data = w.get_behavioral_scoring_data(video,BEHAVIORS)
    beh_list = w.get_scoring_list(BEHAVIORS)
    dist = w.compute_distance(data)
    speed_mouse_1 = w.compute_speed(data,'M1')
    speed_mouse_2 = w.compute_speed(data,'M2')
    
    _set_plot_style()
    plt.figure(figsize=(13, 3))
    for beh in beh_list:
        color = _get_behavior_color(beh)
        style = _get_line_style(beh)
        func = lambda x: 3000 if x == beh else -100
        plt.plot(behavioral_scoring_data.Default[start:end].apply(func),
                 label=beh, color=color, linestyle=style)
    plt.plot(dist[start:end] / PIXEL_TO_CM, label='Distance', color='red')
    plt.ylim((0, 80))
    plt.axhline(y=300, color='black', linestyle='--')
    _set_plot_labels(start, end, 45)
    plt.show()

    plt.figure(figsize=(13, 3))
    for beh in beh_list:
        color = _get_behavior_color(beh)
        style = _get_line_style(beh)
        plt.plot(behavioral_scoring_data.Default[start:end].apply(func),
                 label=beh, color=color, linestyle=style)

    plt.plot(speed_mouse_1.loc[start:end] / PIXEL_TO_CM,
             label='speed 1', color='black', alpha=1, linestyle='dotted')
    plt.plot(speed_mouse_2.loc[start:end] / PIXEL_TO_CM,
             label='speed 2', color='black', alpha=1)
    plt.ylim((0, 80))
    _set_plot_labels(start, end, 45)
    plt.show()
    

def _set_plot_style() -> None:
    '''
        Sets axes style for plot_behavioral_data_example(...)
    '''
    rcParams['axes.spines.bottom'] = True
    rcParams['axes.spines.left'] = True
    rcParams['axes.spines.right'] = False
    rcParams['axes.spines.top'] = False
    rcParams['axes.edgecolor'] = 'black'


def _set_plot_labels(start: int, end: int, frame_rate: int) -> None:
    '''
        Sets labels for plot_behavioral_data_example(...)
        
        Args: 
            start: start value for x ticks
            end: end value for x ticks
            frame_rate: step between to consecutive x ticks
    '''
    plt.xlabel('Time (s)', fontsize=10)
    plt.ylabel('Distance (cm)', fontsize=10)
    plt.xticks(ticks=list(range(start, end + 1, frame_rate)),
               labels=[round(i / frame_rate) for i in range(0, 1351, frame_rate)],
               fontsize=10)
    plt.yticks(fontsize=10)
    plt.xlim((start, end))


    
def plot_window(curve_list: list[pd.Series], curve: pd.Series, title: str) -> None:
    '''
        Plots the curves produced in compute_window(..)
        
        Args: 
            curve_list: list of curves to be represented as overlapping 
                in the same plot
            curve: single curve to be plotted together with curve_list. 
                Generally, this curve represents the average curve
            title: plot title
    '''
    if curve_list.shape[0] > 0:
        plt.plot(curve_list.T, color = 'grey', alpha = 0.1)

        if 'speed' in title:
            start = 180
            max_y = 140
            step = 45
            plt.vlines(start,0,max_y,color = 'black')
            plt.ylim((-2,max_y))
            plt.ylabel('Speed (cm/s)')
        elif 'distance' in title:
            start = 180
            step = 45
            plt.vlines(start,0,140,color = 'black')
            plt.ylim((-4,140))
            plt.ylabel('Distance (cm)')
        plt.plot(curve, color = 'red')

        plt.xlabel('Time (s)',fontsize = 12)
        labels = [round((i - start)/45) for i in range(0,curve.shape[0],step)]
        plt.xticks(ticks = list(range(0,curve.shape[0],step)),
                   labels = labels)

    else:
        print("empty curve list")
    
    
    
    
    
try:
    # plot entire speed and distance trace of a recording
    print('\nPlot speed and distance over time')
    plot_behavioral_data('CD1-B1-MS_US-T1.2.csv')
    
    # plot traces within one interval
    video = 'CD1-B6-UN_LS-T1.2.csv'
    plot_behavioral_data_example(2170, 3520, video)
    
    
    line = 'C57j'
    time = 'T1'
    left_length = 180
    right_length = 180

    curve_to_visualize = 'distance'
    behavior = 'flights_without_chases'
    align_criterium = 'max_slope'

    if align_criterium: crit_title = ' -  alignment by ' + align_criterium
    else: crit_title = ' - no aligment'

    video_list_all = w.get_video_list()
    video_list = [video for video in video_list_all if time in video and line in video]
    
    print('Computing average distance and speed traces around flight onset for all listed recordings...\n')
    curve_list = w.compute_window(video_list, behavior, left_length, 
                                  right_length, curve_to_visualize, 
                                  align_criterium)    
    
    mean_curve = np.nanmean(curve_list, axis = 0) 

    plt.figure(figsize = (6,6))
    plot_window(curve_list, mean_curve, curve_to_visualize)
    plt.show()
    
    plt.figure(figsize = (6,6))   
    st_error = sem(curve_list)
    plt.plot(mean_curve)
    x = [i for i in range(left_length + right_length+1)]
    plt.fill_between(x,mean_curve+st_error, mean_curve-st_error,alpha = 0.4)
    plt.show()

except: 
    print('Failed')