"""
Co-habitation time spent within short distance

In this script, we measure the duration that two mice from the same pair spend in close proximity to each other. "Close" is defined as follows:
- They must be in the same ROI.
- The Euclidean distance between them must be less than a predefined threshold, referred to here as the "radius."


Input data:

|   |video         |animal |roi      |entry|exit|
|-  |-             |-      |-        |-    |-   |
|0  |C57j-B6-US_MS |UM     |Up-corn1 |1214 |1477|
|1  |C57j-B6-US_MS |UM     |Up-corn1 |1690 |2500|
|2  |C57j-B6-US_MS |UM     |Up-corn1 |2570 |2598|
|3  |C57j-B6-US_MS |UM     |Up-corn1 |3688 |3737|
|4  |C57j-B6-US_MS |UM     |Up-corn1 |7210 |7301|
|.. |..            |..     |..       |..   |..  |  


"""


import importlib
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import window_analysis as d

PIXEL_TO_CM = 15.4
ROIS = ['Down-HF','Down-comp','Down-corn1','Down-corn2','Down-corn3','Down-corn4','Down-nest','Down-HT',
        'Up-HF','Up-comp','Up-corn1','Up-corn2','Up-corn3','Up-corn4','Up-nest','Up-HT']



def delete_hiding_noise(data: pd.DataFrame) -> pd.DataFrame:
    '''
        Sometimes, a mouse moving very close to an ROI is incorrectly detected as being
        inside this ROI for a few frames. The following function deletes those rows where
        the mouse is considered to be in one ROI for less than one second,
        aiming to reduce noise in the dataset.
        
        Args: 
            data: dataset containing ROI information for each mouse along with the 
                  corresponding entry and exit time frames.
        Returns: 
            denoised version of the input dataset
    '''
    indices = data[data.exit - data.entry <= FRAMERATE].index.values.tolist()
    data.drop(indices, inplace = True)
    data.reset_index(inplace = True)
    return data



def create_rois_per_frame(data: pd.DataFrame):
    """
        Creates a dataset of the following format:
        |                         | C57j-B6-UN_LS-T1.1-LM | C57j-B6-US_MS-T1.2-LM | C57j-B6-UN_LS-T1.2-UM | ... | CD1-B6-UN_LS-T1.1-LM  | CD1-B6-UN_LS-T1.2-UM  |
        |-------------------------|-----------------------|-----------------------|-----------------------|-----|-----------------------|-----------------------|
        | 0                       | Down-corn2            | Down-comp             | Up-comp               | ... | Up-nest               | Up-corn4              |
        | 1                       | Down-corn2            | Down-comp             | Up-comp               | ... | Up-nest               | Up-corn4              |
        | 2                       | Down-corn2            | Down-comp             | Up-comp               | ... | Up-nest               | Up-corn4              |
        | 3                       | Down-corn2            | Down-comp             | Up-comp               | ... | Up-nest               | Up-corn4              |
        | 4                       | Down-corn2            | Down-comp             | Up-comp               | ... | Up-nest               | Up-corn4              |
        | ...                     | ...                   | ...                   | ...                   | ... | ...                   | ...                   | 
    
        Args:
            data: dataset containing ROI information for each mouse along with the 
                  corresponding entry and exit time frames.
        Returns: 
            Reshaped version of data indicating ROIs information for each mouse and for each frame
    """

    data = delete_hiding_noise(data)
    videos = data.video.unique()
    new_dataset = pd.DataFrame()

    for video in tqdm(videos):
        for mouse in ['UM','LM']:
            column_name = video + '-' + mouse
            column = pd.Series([None for i in range(27000)], name = column_name)
            for roi in rois:
                data1 = data[data.video == video]
                data2 = data1[data1.roi == roi]
                data3 = data2[data2.animal == mouse]
                if data3.shape[0] > 0:
                    for i in range(data3.shape[0]):
                        start = data3.entry.iloc[i]
                        end = data3.exit.iloc[i]
                        column.iloc[start:end+1] = [roi for j in range(start,end+1,1)]
            new_dataset = pd.concat([new_dataset, column],axis=1)
    return new_dataset



def replace_names(rois_per_frame: pd.DataFrame):
    """
        Adjust roi names:
        We temporarily employ new roi names by considering the up/down corners as <br>
        part of their respective up/down compartments.
        
        Args:
            rois_per_frame: data indicating ROIs information for each mouse and for each frame
        Returns:
            same dataset with simplified ROIs nomenclature
    """
    for old_name in ['Down-comp','Down-corn1','Down-corn2','Down-corn3','Down-corn4']:    
        rois_per_frame.replace(old_name,'downcomp', inplace = True)
    for old_name in ['Up-comp','Up-corn1','Up-corn2','Up-corn3','Up-corn4']:    
        rois_per_frame.replace(old_name,'upcomp', inplace = True)
    return rois_per_frame




def cohabitation_distance(radius: int, times: str, rois_per_frame: pd.DataFrame) -> dict:
    """
    Computes portion of time spent in the same ROI and within radius distance
    Args:
        radius: max distance
        times:  'T1' (early) or 'T2' (late)
        rois_per_frame: data indicating ROIs information for each mouse and for each frame
    Returns: 
        time_close_dict: dictionary storing time spend close to each other
                        by each recording pair for all strains.
    
    """
    rois_per_frame = replace_names(rois_per_frame)
    time_close_dict = {}
    for line in ['CD1','Hyb','C57j']:
        time_cose_line_dict = {}

        for time in times: 
            columns = [c for c in rois_per_frame.columns.tolist() if (line in c and time in c)]
            n = len(columns)
            time_close = []

            for i in range(0,n,2):
                video = columns[i][:-3] + '.csv'
                data = d.get_data(video)
                dist = d.compute_distance(data) / PIXEL_TO_CM
                dist_filter = dist.apply(lambda x: 1 if x < radius else 0) 

                cond_same_roi = rois_per_frame[columns[i]] == rois_per_frame[columns[i+1]]
                cond_same_roi.apply(lambda x: 1 if x else 0)

                num_frames_close = np.dot(cond_same_roi[0:dist.shape[0]],dist_filter)
                tot_time = dist.shape[0]
                time_close.append(num_frames_close/tot_time)

            time_cose_line_dict[time] = time_close
        time_close_dict[line] = time_cose_line_dict
    return time_close_dict




def plot_cohabitatio_distance(times: str, time_close_dict: dict):
    """
    Plots mean portion of time spent in the same ROI and within radius distance
    for each line.
     Args:
        time_close_dict: dictionary storing time spend close to each other
                        by each recording pair for all strains.
        times:  'T1' (early) or 'T2' (late)
    
    """
    plt.figure(figsize = (4,3))

    for line in ['CD1','Hyb','C57j']:
        means = []
        for time in times:
            means.append(np.mean(time_close_dict[line][time]))

        plt.plot([0,1],means, label = line)
        plt.scatter([0,1],means,s=100)
        title = 'Radius distance: ' + str(radius) + 'cm'
        plt.title(title)
        plt.xticks(ticks = [i for i in range(len(times))],labels = times, fontsize = 15)
        plt.ylim((0,0.5))
        plt.xlim((-0.2,1.2))
        plt.legend()
    plt.show()



try:
    data = pd.read_csv('input_data.csv',low_memory=False)  # change path name for input data
    rois_per_frame = create_rois_per_frame(data)
    time_close_dict = cohabitation_distance(10,"T1", rois_per_frame)
    plot_cohabitatio_distance("T1", time_close_dict)

except:
    print("Failed")
