""" 

Module for generating a dataset where for each mouse we compute 
details about time spent is certain rois for hiding quantification
    
Input data structure:

|   |video         |animal |roi      |entry|exit|
|-  |-             |-      |-        |-    |-   |
|0  |C57j-B6-US_MS |UM     |Up-corn1 |1214 |1477|
|1  |C57j-B6-US_MS |UM     |Up-corn1 |1690 |2500|
|2  |C57j-B6-US_MS |UM     |Up-corn1 |2570 |2598|
|3  |C57j-B6-US_MS |UM     |Up-corn1 |3688 |3737|
|4  |C57j-B6-US_MS |UM     |Up-corn1 |7210 |7301|
|.. |..            |..     |..       |..   |..  |    
    
    
"""

import numpy as np
import pandas as pd


FRAMERATE = 45
N = 27000    # Total number of frames in a 10-minute recording


def delete_hiding_noise(data: pd.DataFrame) -> pd.DataFrame:
    '''
        Sometimes, a mouse moving very close to an ROI is incorrectly detected as being
        inside this ROI for a few frames. The following function deletes those rows where
        the mouse is considered to be in one ROI for less than one second,
        aiming to reduce noise in the dataset.
        
        Args: 
            data: dataset containing ROI information for each mouse along with the 
                  corresponding entry and exit time frames.
        Returns: 
            denoised version of the input dataset
    '''
    indices = data[data.exit - data.entry <= FRAMERATE].index.values.tolist()
    data.drop(indices, inplace = True)
    data.reset_index(inplace = True)
    return data


def hiding_and_unreachable_vectors(data: pd.DataFrame) -> pd.DataFrame:
    '''
        This function computes: 
         - a vector containing 1 for each frame when the mouse is hiding, i.e. 
         when it is in a corner (corn).
         - a vector containing 1 for each frame when the mouse is unreachable, 
         i.e. it is in any high-position ROI (H-..).
         
         Args: 
             data: dataset containing ROI information for each mouse along with the 
                  corresponding entry and exit time frames.
        Returns: 
            hiding: binary data with 1 for frames when the mouse is hiding
            unreachable: binary data with 1 for frames when the mouse is
                        unreachable by the other mouse
            
    '''
    hiding = np.zeros(N*11)
    unreachable = np.zeros(N*11)

    for i in range(data.shape[0]):
        if 'corn' in data.roi.iloc[i] or 'H' in data.roi.iloc[i]:
            for j in range(data.entry.iloc[i],data.exit.iloc[i],1):
                hiding[j] = 1
                if 'H' in data.roi.iloc[i]:
                    for h in range(data.entry.iloc[i],data.exit.iloc[i],1):
                        unreachable[h] = 1
    return hiding, unreachable






def time_in_rois(paths: str) -> list[list]:
    '''
        Here, for each video, we first apply 'delete_hiding_noise' to filter out
        the noise related to hiding time.
        Then, for each mouse belonging to the same recording, we compute: 
        - the total time of the recording
        - the total time spent hiding
        - the portion of time spent in different regions 
          (other compartment, other nest, other HF)
        
        Args:
            paths: input data path
        Returns: 
            vector_list: list of vectors. Each vector contains specific
                        time measurements for each single mouse
      

    '''

    vector_list = []

    for path in paths:
        data = pd.read_csv(path)
        data = delete_hiding_noise(data)
        names = data.video.unique()

        for file_name in names:
            for mouse in ['UM','LM']:

                if mouse == 'UM': 
                    other_comp = 'Down'
                else: 
                    other_comp = 'Up'

                vector = [file_name+'-'+mouse]
                d1 = data[data.video == file_name]
                d1 = d1[d1.animal == mouse]

                t1 = (d1[d1.roi == 'Up-comp']['exit'] - d1[d1.roi == 'Up-comp']['entry']).sum()
                t2 = (d1[d1.roi == 'Down-comp']['exit'] - d1[d1.roi == 'Down-comp']['entry']).sum()
                total_time1 = t1 + t2

                total_time_hidden1 = 0
                for roi in ['Up-HF', 'Down-HF', 'Up-HT', 'Down-HT', 'Up-corn1','Up-corn2',
                            'Up-corn3','Up-corn4','Down-corn1','Down-corn2','Down-corn3',
                            'Down-corn4' ]:
                    t3 = (d1[d1.roi == roi]['exit'] - d1[d1.roi == roi]['entry']).sum()
                    total_time_hidden1 = total_time_hidden1 + t3

                t_exit = d1[d1.roi == other_comp + '-comp']['exit']
                t_entry = d1[d1.roi == other_comp + '-comp']['entry']
                total_time_other_comp1 = (t_exit - t_entry).sum()

                t_exit = d1[d1.roi == other_comp + '-nest']['exit']
                t_entry =  d1[d1.roi == other_comp + '-nest']['entry']
                total_time_other_nest1 = (t_exit - t_entry).sum()

                t_exit = d1[d1.roi == other_comp + '-HF']['exit']
                t_entry = d1[d1.roi == other_comp + '-HF']['entry']
                total_time_other_hf1 = (t_exit - t_entry).sum()

                total_time_hidde_other_comp1 = 0
                for roi in [other_comp + '-HF', other_comp + '-HT', other_comp + '-corn1',
                            other_comp + '-corn2', other_comp + '-corn3',other_comp + '-corn4']:
                    t4 =  (d1[d1.roi == roi]['exit'] - d1[d1.roi == roi]['entry']).sum()
                    total_time_hidde_other_comp1 = total_time_hidde_other_comp1 + t4

                portion_time_other_comp1 = round(total_time_other_comp1/total_time1,2)
                portion_time_other_nest_hf1 = total_time_other_nest1 - total_time_other_hf1
                portion_time_other_nest_hf1 = portion_time_other_nest_hf1 / total_time1
                portion_time_other_nest_hf1 = round(portion_time_other_nest_hf1,2)
                portion_time_hidden1 = round(total_time_hidden1/total_time1,2)
                portion_time_hidde_other_comp1 = round(total_time_hidde_other_comp1/total_time1,2)
                vector = vector + [portion_time_other_comp1,portion_time_other_nest_hf1,
                                   portion_time_hidden1,portion_time_hidde_other_comp1]
                vector_list.append(vector)

    return vector_list



def traveled_distance(paths: str) -> list[list]:
    '''    
        Here, we extract the information about the distance traveled by each mouse
        
        Args:
            paths: input data path
        Returns: 
            vectors: list containing the recording and mouse names, 
                     and the total distance traveled by the mouse 
                     during the recording.
    '''

    vectors = []

    for path in paths:
        data = pd.read_csv(path)
        names = data.video.unique()

        for file_name in names:
            for mouse in ['UM','LM']:
                vector = [file_name+'-'+mouse]

                cond1 = data.video == file_name
                cond2 = data.animal == mouse
                cond3 = data.measure == 'Distance (cm)'
                distance = data[cond1][cond2][cond3].value.values[0]
                vector.append(distance)
            vectors.append(vector)
    return vectors



def cumulative_time_in_rois(paths: str) -> pd.DataFrame:

    '''
        In the following snippet, for each video, we first apply 'delete_hiding_noise'
        to filter out the noise related to hiding time.
        Then, for each mouse belonging to the same recording, we compute: 
        - the cumulative time spent hiding
        - the cumulative time spent in unreachable areas

        For T2 videos, it identifies a restart frame and adjusts the cumulative 
        sums accordingly.
        
        Args:
            paths: input data path
        Returns: 
            dataframe: Each raw of the dataframe contains specific
                        time measurements for each single mouse
    '''

    for path in paths:
        data = pd.read_csv(path)
        data = delete_hiding_noise(data)
        names = data.video.unique()

        for file_name in names:
            for mouse in ['UM','LM']:

                d = data[data.video == file_name]
                d = d[d.animal == mouse]

                hiding, unreachable = hiding_and_unreachable_vectors(d)
                cumsum1 = np.cumsum(hiding)
                cumsum2 = np.cumsum(unreachable)

                time_in_seconds = [round(i/FRAMERATE,4) for i in range(N*11)]
                dataframe = pd.DataFrame({'frames': list(range(N*11)),
                                          'time_in_seconds': time_in_seconds,'hiding': hiding, 
                                          'unreachable': unreachable, 'hiding_cumsum': cumsum1, 
                                          'unreachable_cumsum': cumsum2})           
                total_time = (d[(d.roi == 'Up-comp') | (d.roi == 'Down-comp')]['exit'] -
                              d[(d.roi == 'Up-comp') | (d.roi == 'Down-comp')]['entry']).sum()

                cumsum1_portion = dataframe.hiding_cumsum / total_time
                cumsum2_portion = dataframe.unreachable_cumsum / total_time

                dataframe['relative_hiding_cumsum'] = cumsum1_portion
                dataframe['relative_unreachable_cumsum'] = cumsum2_portion

                if '-T1.2' in file_name:
                    d1 = data[data.video == file_name]
                    d1 = d1[d1.animal == mouse]
                    restart_frame = d1.exit.max()

                    d_hiding_cumsum = dataframe.hiding_cumsum[dataframe.frames == restart_frame]
                    restart_h_cumsum = d_hiding_cumsum[restart_frame]

                    condition = dataframe.frames == restart_frame
                    d_unreachable_cumsum = dataframe.unreachable_cumsum[condition]
                    restart_unreachable_cumsum = d_unreachable_cumsum[restart_frame]

                    condition = dataframe.frames == restart_frame
                    d_restart_hiding = dataframe.relative_hiding_cumsum[condition]
                    restart_relative_hidden_cumsum = d_restart_hiding[restart_frame]

                    condition = dataframe.frames == restart_frame
                    d_restart_unreachable = dataframe.relative_unreachable_cumsum[condition]
                    restart_relative_unreachable_cumsum = d_restart_unreachable[restart_frame]

                    dataframe.drop(list(range(restart_frame + 1,N*9,1)), inplace = True)

                    d_h_subset = dataframe.hiding_cumsum.iloc[restart_frame + 1 :]
                    dataframe.hiding_cumsum.iloc[restart_frame + 1 :] = d_h_subset.apply(lambda x: x - restart_h_cumsum)

                    d_subset = dataframe.unreachable_cumsum.iloc[restart_frame + 1 :]
                    dataframe.unreachable_cumsum.iloc[restart_frame + 1 :] = d_subset.apply(lambda x: x - restart_unreachable_cumsum)

                    d_subset = dataframe.relative_hiding_cumsum.iloc[restart_frame + 1 :]
                    dataframe.relative_hiding_cumsum.iloc[restart_frame + 1 :] = d_subset.apply(lambda x: x - restart_relative_hidden_cumsum)

                    d_subset = dataframe.relative_unreachable_cumsum.iloc[restart_frame + 1 :]
                    dataframe.relative_unreachable_cumsum.iloc[restart_frame + 1 :] = d_subset.apply(lambda x: x - restart_relative_unreachable_cumsum)
    return dataframe
